package teste.heranca;

import infra.DAO;
import modelo.heranca.Aluno;
import modelo.heranca.AlunoBolsista;

public class NovoAluno {
    public static void main(String[] args) {
        DAO<Aluno> alunoDAO = new DAO<>();

        Aluno a = new Aluno(123L, "Joao");
        AlunoBolsista a2 = new AlunoBolsista(345L, "Maria", 1000);

        alunoDAO.incluirAtomico(a);
        alunoDAO.incluirAtomico(a2);

        alunoDAO.fechar();
    }
}

