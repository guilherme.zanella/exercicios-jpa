package teste.umpramuitos;

import infra.DAO;
import modelo.basico.Produto;
import modelo.umpramuitos.ItemPedido;
import modelo.umpramuitos.Pedido;

import java.util.Scanner;

public class NovoPedido {
    public static void main(String[] args) {
        DAO<Object> dao = new DAO<>();

        Scanner sc = new Scanner(System.in);

        System.out.print("Informe a descrição do produto: ");
        String nome = sc.nextLine();
        System.out.print("Informe o valor do produto: ");
        double preco = sc.nextDouble();
        System.out.print("Informe a quantidade: ");
        int qtd = sc.nextInt();

        Produto pr = new Produto(nome, preco);
        Pedido pe = new Pedido();
        ItemPedido ip = new ItemPedido(pe, pr, qtd);

        System.out.println(qtd + " " + nome + "(s) - Total: " + qtd * preco);

        dao.abrirT().incluirT(pr).incluirT(pe).incluirT(ip).fecharT().fechar();
    }
}

