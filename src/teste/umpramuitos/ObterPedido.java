package teste.umpramuitos;

import infra.DAO;
import modelo.umpramuitos.ItemPedido;
import modelo.umpramuitos.Pedido;

import java.util.Scanner;

public class ObterPedido {
    public static void main(String[] args) {
        DAO<Pedido> dao = new DAO<>(Pedido.class);
        Scanner sc = new Scanner(System.in);

        System.out.print("Informe o ID do pedido: ");
        long id = sc.nextLong();

        Pedido pe = dao.obterPorId(id);

        if(pe != null) {
            for(ItemPedido item: pe.getItens()) {
                System.out.println(item.getProduto().getNome() + " - R$ " + item.getPreco());
                System.out.println("Quantidade: " + item.getQuantidade());
                System.out.println("Valor total: R$ " + item.getPreco() * item.getQuantidade());
            }
        } else {
            System.out.println("Nenhum produto corresponde ao ID.");
        }

        dao.fechar();
        sc.close();
    }
}

