package teste.umpraum;

import infra.DAO;
import modelo.umpraum.Assento;
import modelo.umpraum.Cliente;

public class ObterClienteAssento {
    public static void main(String[] args) {
        DAO<Cliente> daoC = new DAO<>(Cliente.class);
        Cliente c = daoC.obterPorId(1L);

        System.out.println(c.getAssento().getNome());
        daoC.fechar();

        DAO<Assento> daoA = new DAO<>(Assento.class);
        Assento a = daoA.obterPorId(1L);

        System.out.println(a.getCliente().getNome());
        daoA.fechar();
    }
}

