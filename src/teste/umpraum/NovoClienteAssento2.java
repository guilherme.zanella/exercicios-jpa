package teste.umpraum;

import infra.DAO;
import modelo.umpraum.Assento;
import modelo.umpraum.Cliente;

public class NovoClienteAssento2 {
    public static void main(String[] args) {
        Assento a = new Assento("4D");
        Cliente c = new Cliente("Rodrigo", a);

        DAO<Cliente> dao = new DAO<>(Cliente.class);

        dao.incluirAtomico(c);
    }
}

