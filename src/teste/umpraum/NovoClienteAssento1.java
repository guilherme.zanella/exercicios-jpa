package teste.umpraum;

import infra.DAO;
import modelo.umpraum.Assento;
import modelo.umpraum.Cliente;

public class NovoClienteAssento1 {
    public static void main(String[] args) {
        Assento a = new Assento("16A");
        Cliente c = new Cliente("Carlos", a);

        DAO<Object> dao = new DAO<>();

        dao.abrirT().incluirT(a).incluirT(c).fecharT().fechar();
    }
}

