package teste.basico;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import modelo.basico.Usuario;

import java.util.Scanner;

public class NovoUsuario {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicios-jpa");
        EntityManager em = emf.createEntityManager();
        Scanner entrada = new Scanner(System.in);

        System.out.print("Informe o nome do novo usuário: ");
        String nome = entrada.nextLine();

        System.out.print("Informe o email do novo usuário: ");
        String email = entrada.nextLine();

        Usuario u = new Usuario(nome, email);

        em.getTransaction().begin();
        em.persist(u);
        em.getTransaction().commit();

        em.close();
        emf.close();
        entrada.close();
    }
}

