package teste.basico;

import infra.DAO;
import modelo.basico.Produto;

import java.util.Scanner;

public class NovoProduto {
    public static void main(String[] args) {
        DAO<Produto> dao = new DAO<>(Produto.class);
        Scanner sc = new Scanner(System.in);

        System.out.print("Informe a descrição do produto: ");
        String nome = sc.nextLine();
        System.out.print("Informe o valor do produto: ");
        double preco = sc.nextDouble();

        Produto p = new Produto(nome, preco);

        dao.incluirAtomico(p).fechar();

        System.out.println("Id do produto: " + p.getId());

        sc.close();
    }
}

