package teste.basico;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import modelo.basico.Usuario;

import java.util.Scanner;

public class AlterarUsuario1 {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicios-jpa");
        EntityManager em = emf.createEntityManager();
        Scanner en = new Scanner(System.in);

        em.getTransaction().begin();

        System.out.print("Informe o ID do usuário que deseja alterar: ");
        long id = en.nextLong();
        en.nextLine();

        Usuario u = em.find(Usuario.class, id);

        if(u != null){
            System.out.print("Informe o novo nome do usuario: ");
            String nome = en.nextLine();

            System.out.print("Informe o novo email do usuario: ");
            String email = en.nextLine();

            u.setNome(nome);
            u.setEmail(email);

            em.merge(u);

            em.getTransaction().commit();
        } else {
            System.out.println("Nenhum usuário corresponde ao ID.");
        }

        em.close();
        emf.close();
        en.close();
    }
}

