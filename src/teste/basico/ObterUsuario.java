package teste.basico;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import modelo.basico.Usuario;

import java.util.Scanner;

public class ObterUsuario {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicios-jpa");
        EntityManager em = emf.createEntityManager();
        Scanner entrada = new Scanner(System.in);

        System.out.print("Informe o id do usuário: ");
        Long id = entrada.nextLong();

        Usuario u = em.find(Usuario.class, id);
        System.out.println(u.getNome());

        em.clear();
        emf.close();
        entrada.close();
    }
}

